import 'dart:convert';

import 'package:emp_v2/authentication/auth.dart';
import 'package:emp_v2/login/login.dart';
import 'package:emp_v2/utils/colors_util.dart';
import 'package:emp_v2/utils/message_dialog.dart';
import 'package:emp_v2/utils/progress_dialog.dart';
import 'package:emp_v2/utils/shared_prefrence.dart';
import 'package:emp_v2/utils/validate.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

class SignUpPage extends StatefulWidget {
  String emailStr = "";

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey key = GlobalKey();

  Map response = new Map();
  String email = '';
  String password = '';
  String message = '';
  String firstname = '';
  String lastname = '';
  String errorMsg = '';
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _isSelected = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: darkBlueColor,
        resizeToAvoidBottomInset: false,
        body: Container(
            height: 100.h,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/splash_bg.png'),
                  fit: BoxFit.fill),
            ),
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 8.0),
                      child: Text(
                        'Create your account',
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: GoogleFonts.montserrat(
                          fontSize: 22.sp,
                          fontWeight: FontWeight.w300,
                          color: blueColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        'Create an account with your official ghrix.com email address',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w300,
                          color: subHeadingColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 100.w,
                            height: 55.0,
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                            decoration: BoxDecoration(
                                color: grayColor,
                                borderRadius: BorderRadius.circular(15.0)),
                            child: TextFormField(
                              controller: _emailController,
                              textInputAction: TextInputAction.next,
                              maxLines: 1,
                              validator: (value) {
                                email = value.trim();
                                return Validate.validateEmail(email);
                              },
                              style: GoogleFonts.poppins(
                                  color: textColor, fontSize: 14.sp),
                              decoration: InputDecoration(
                                hintText: "Email",
                                hintStyle: GoogleFonts.poppins(
                                    fontSize: 14.sp, color: subHeadingColor),
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                errorStyle: TextStyle(
                                    height: 0, color: Colors.transparent),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 15.0, horizontal: 10.0),
                              ),
                            ),
                          ),
                          Container(
                            width: 100.w,
                            height: 55.0,
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                            decoration: BoxDecoration(
                                color: grayColor,
                                borderRadius: BorderRadius.circular(15.0)),
                            child: TextFormField(
                              controller: _passwordController,
                              obscureText: true,
                              maxLines: 1,
                              validator: (value) {
                                password = value.trim();
                                return Validate.validatePass(value);
                              },
                              style: GoogleFonts.poppins(
                                  color: textColor, fontSize: 14.sp),
                              decoration: InputDecoration(
                                hintText: "Password",
                                hintStyle: GoogleFonts.poppins(
                                    fontSize: 14.sp, color: subHeadingColor),
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                errorStyle: TextStyle(
                                    height: 0, color: Colors.transparent),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 15.0, horizontal: 10.0),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 20.0, vertical: 10.0),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(
                                      children: <TextSpan>[
                                        TextSpan(
                                            text: "I have read the",
                                            style: GoogleFonts.poppins(
                                                color: subHeadingColor,
                                                fontWeight: FontWeight.w300,
                                                fontSize: 12.sp)),
                                        TextSpan(
                                            text: " Privacy Policy",
                                            style: GoogleFonts.poppins(
                                              color: blueColor,
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w300,
                                            )),
                                      ],
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        _isSelected = !_isSelected;
                                      });
                                    },
                                    child: AnimatedContainer(
                                      margin: EdgeInsets.all(4),
                                      duration: Duration(milliseconds: 500),
                                      curve: Curves.fastLinearToSlowEaseIn,
                                      decoration: BoxDecoration(
                                          color: _isSelected
                                              ? blueColor
                                              : Colors.transparent,
                                          borderRadius:
                                              BorderRadius.circular(3.0),
                                          border: Border.all(
                                            color: Color(0xFFA1A4B2),
                                            width: 1.5,
                                          )),
                                      width: 18,
                                      height: 18,
                                      child: _isSelected
                                          ? Icon(
                                              Icons.check,
                                              color: Colors.white,
                                              size: 14,
                                            )
                                          : null,
                                    ),
                                  )
                                ]),
                          ),
                          Container(
                            width: 100.w,
                            height: 55.0,
                            decoration: BoxDecoration(
                                color: blueColor,
                                borderRadius: BorderRadius.circular(15.0)),
                            margin: EdgeInsets.symmetric(
                                horizontal: 25.0, vertical: 10.0),
                            child: InkWell(
                              onTap: () async {
                                email = _emailController.text.trim();
                                password = _passwordController.text.trim();
                                if (Validate.validateEmail(email) != null) {
                                  DialogUtils.showCustomDialog(context,
                                      title: 'Signup',
                                      message: Validate.validateEmail(email),
                                  );
                                } else if (Validate.validatePass(password) !=
                                    null) {
                                  DialogUtils.showCustomDialog(context,
                                      title: 'Signup',
                                      message: Validate.validatePass(password),
                                      );
                                } else if (!_isSelected) {
                                  DialogUtils.showCustomDialog(context,
                                      title: 'Signup',
                                      message: 'Please read and accept the Privacy Policy ',
                                    okBtnFunction: (){}
                                      );
                                } else {
                                  signUpAPI();
                                  print("Successful");
                                }

                                /* Navigator.pushReplacement(context,
                      MaterialPageRoute(builder: (context) => SignUpPage()));*/
                              },
                              child: Center(
                                child: Text(
                                  "GET STARTED",
                                  style: GoogleFonts.montserrat(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w300,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 30),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginPage()));
                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                                text: "ALREADY HAVE AN ACCOUNT?  ",
                                style: GoogleFonts.poppins(
                                    color: subHeadingColor,
                                    fontWeight: FontWeight.w300,
                                    fontSize: 12.sp)),
                            TextSpan(
                                text: "LOGIN",
                                style: GoogleFonts.poppins(
                                  color: blueColor,
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w300,
                                )),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )));
  }

  Future signUpAPI() async {
    Dialogs.showLoadingDialog(context, key);
    print("Data : $email $password");
    var param = {"email": email, "password": password, "name": ""};
    Auth().addData(Auth.register, param).then((response) {
      Navigator.of(key.currentContext).pop();
      var jsonResponse = json.decode(response.body);
      print("Response code : ${response.body}");

        if (jsonResponse['success'] == true) {
          SharedPreferencesHelper.saveUserId(jsonResponse['user']['id']);
          SharedPreferencesHelper.saveEmail(jsonResponse['user']['email']);
          SharedPreferencesHelper.isFirstTime(false);
        }
        DialogUtils.showCustomDialog(context,
            title: 'Signup',
            message: jsonResponse['message'],
            okBtnFunction: () {
              Navigator.of(context).pop();
            }
            );

    }, onError: (error) {
      Navigator.of(key.currentContext).pop();
      print("Error == $error");
      Map<String, dynamic> data =
          new Map<String, dynamic>.from(json.decode(error));

      DialogUtils.showCustomDialog(context,
          title: 'Error',
          message: data["message"]['email'][0] ?? 'Something went wrong..',
          okBtnFunction: (){}
      );
    });
  }
}
