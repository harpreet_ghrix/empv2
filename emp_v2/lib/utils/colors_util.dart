import 'package:flutter/material.dart';

const textLightColor = Color(0x80000000);
const whiteColor = Color(0xFFFFFFFF);
const blueColor = Color(0xFF1C59A7);
const darkBlueColor = Color(0xFF161D31);
const blueTransColor = Color(0xEE005FF9);
const lightBlueColor = Color(0xFFF3F8FF);
const grayColor = Color(0xFFD4D4D4);
const textColor = Color(0xFF3F414E);
const subHeadingColor = Color(0xFF96A7AF);