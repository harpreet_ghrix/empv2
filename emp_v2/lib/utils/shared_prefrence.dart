
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHelper {
  ///
  /// Instantiation of the SharedPreferences library
  ///
  final String _kNotificationsPrefs = "allowNotifications";
  final String _kSortingOrderPrefs = "sortOrder";
  static SharedPreferences sharedPrefs;

  init() async {
    if (sharedPrefs == null) {
      sharedPrefs = await SharedPreferences.getInstance();
    }
  }

  String get username => sharedPrefs.getString("NAME")  ?? "";

  set username(String value) {
    sharedPrefs.setString("NAME", value);
  }

  SharedPreferencesHelper instance = SharedPreferencesHelper();

  static Future<bool> saveUserId(int value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setInt("USERID", value);
  }

  static Future<int> getUserId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getInt("USERID") ?? 0;
  }

  static Future<bool> saveUserType(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString("USERTYPE", value);
  }

  static Future<String> getUserType() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString("USERTYPE") ?? '';
  }

  static Future<bool> saveDeviceToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString("DEVICETOKEN", value);
  }

  static Future<String> getDeviceToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString("DEVICETOKEN") ?? '0';
  }

  static Future<bool> saveToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString("TOKEN", value);
  }

  static Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString("TOKEN");
  }

  static Future<bool> saveName(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString("NAME", value);
  }

  static Future<String> getName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString("NAME") ?? '';
  }

  static Future<bool> saveBidType(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString("BIDTYPE", value);
  }

  static Future<String> getBidType() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("BIDTYPE") ?? '';
  }

  static Future<bool> saveTech(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString("TECH", value);
  }

  static Future<bool> saveupworkID(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString("UID", value);
  }

  static Future<String> getTech() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("TECH")??  '';
  }

  static Future<bool> saveEmail(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString("EMAIL", value);
  }

  static Future<String> getEmail() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString("EMAIL") ?? '';
  }

  static Future<bool> isFirstTime(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setBool("ISFIRST", value);
  }

  static Future<bool> getisFirstTime() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool("ISFIRST") ?? true;
  }

  static Future<bool> clearPref() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.clear();
  }
}
