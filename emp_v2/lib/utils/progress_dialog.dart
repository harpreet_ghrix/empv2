import 'package:flutter/material.dart';

import 'colors_util.dart';

class Dialogs {
  static Future<void> showLoadingDialog(BuildContext context,
      GlobalKey key) async {
    return showDialog<void>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new WillPopScope(
              onWillPop: () async => false,
              child: Container(
                height: MediaQuery
                    .of(context)
                    .size
                    .height,
                child: Center(
                    key: key,
                    child:
                    CircularProgressIndicator(valueColor:AlwaysStoppedAnimation<Color>(blueColor),)

                ),
              ));
        });
  }
}



