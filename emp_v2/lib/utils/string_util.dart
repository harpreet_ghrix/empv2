import 'package:flutter/material.dart';

/*Login*/
const login = 'Login';
const forgot_password = 'Forgot Password?';
const forgotPass= 'Forgot Password';
const email = 'Email';
const password = 'Password';

/*Home Page*/
const team = "Team";
const current_team = 'Current Team Member';
const add_team_member = 'Add a Team Members';
const listing = 'Listing';
const current_available_listing = 'Current Available Listings';
const invite_only_listing = 'Invite Only Listings';
const hidden_listing = 'Hidden Listings';
const close_listing = 'Close Listings';
const deleted_listing = 'Deleted Listings';
const all_listing = 'All Listings';
const add_new_listing = 'Add a New Listing';
const incomplete_listing = 'Incomplete Listings';
const edit_listing = 'Edit Listing';
const portfolio = 'Portfolio';
const show_portfolio = 'Show Portfolio';
const hidden_portfolio = 'Hidden Portfolio';
const invite_portfolio = 'Invite Only Portfolio';
const add_portfolio = 'Add Portfolio';
const edit_portfolio = 'Edit Portfolio';
const listing_report = 'Listing Reports';
const reports = 'Reports';
const logout = 'Log Out';


const members_list = 'Members List';
const first_name = 'First Name';
const last_name = 'Last Name';
const title = 'Title';
const cell = 'Cell/Phone no.';
const fax = 'Fax';
const office_phone = 'Office Phone';
const company = 'Company';
const city = 'City';
const office = 'Office';
const state = 'State';
const zip = 'Zip';
const street = 'Street';
const province = 'Province';
const street2 = 'Street2';
const country = 'Country';
const lic = 'Licence';

const property_type ='Property Type';
const apartment ='Apartments';
const units ='Units/Keys';
const yoc ='YOC';


const portfolio_name = "Portfolio Name";
const price = "Price";
const cap_rate = "Cap Rate";
const sale_type = "Sale Type";
const property_status = "Property Status";
const num_property = "Number of Properties";
const intended_use = "Intended Use";
const total_building_size = "Total Building Size";
const total_land_area = "Total Land Area";
const hidden = "Hidden";
const invite_only = "Invite Only";
const sales_note = "Sales Notes";


const select_office = "Select Office";
const property_name = "Property Name";
const property_folder = "Property Folder";
const address = "Address";
const total_sf = "Total SF";
const school_name = "School Name";
const beds = "Beds";


const student_housing = "Student Housing";
const manufactured_housing = "Manufactured Housing";
const land = "Development/Land";
const equity_solution = "Equity Solution";





