import 'package:emp_v2/utils/colors_util.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DialogUtils {
  static DialogUtils _instance = new DialogUtils.internal();

  DialogUtils.internal();

  factory DialogUtils() => _instance;

  static void showCustomDialog(BuildContext context,
      {String title,
        String message,
        String okBtnText = "Ok",
        String cancelBtnText = "Cancel",
        Function okBtnFunction}) {
    showDialog(
        context: context,
        builder: (_) {
          return CustomDialog(
              title: title,
              description: message,
              buttonText: okBtnText,
              okBtnFunction: okBtnFunction
          );
          /*  return AlertDialog(
            title: Text(title,style: GoogleFonts.poppins(fontSize: 16.sp,color: blueColor)),
            content: Text(message, style: GoogleFonts.poppins(fontSize: 14.sp,color: subHeadingColor),
              ),
            actions: <Widget>[
              TextButton(
                child: Text(okBtnText,style: GoogleFonts.poppins(fontSize: 14.sp,color: textColor)),
                onPressed:okBtnFunction,),
              TextButton(
                  child: Text(cancelBtnText,style: GoogleFonts.poppins(fontSize: 14.sp,color: textColor)),
                  onPressed: () => Navigator.pop(context))
            ],
          );*/
        });
  }
/* static void showToast(String msg){
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: blueTransColor,
        textColor: Colors.white,
        fontSize: 16.sp);
  }*/
}

class CustomDialog extends StatefulWidget {
  final String title, description, buttonText;
  Function okBtnFunction;

  CustomDialog({@required this.title,
    @required this.description,
    @required this.buttonText,
    this.okBtnFunction});

  @override
  CustomDialogState createState() => CustomDialogState();
}

class CustomDialogState extends State<CustomDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(top: 66.0),
          decoration: new BoxDecoration(
            color: Colors.white, //Colors.black.withOpacity(0.3),
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(16.0),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min, // To make the card compact
            children: <Widget>[
              Text(
                widget.title,
                style: TextStyle(
                  fontSize: 24.0,
                  fontWeight: FontWeight.w300,
                  color: blueColor,
                ),
              ),
              SizedBox(height: 16.0),
              Text(
                widget.description,
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 16.0,
                  color: subHeadingColor,
                ),
              ),
              SizedBox(height: 16.0),
              Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                  onPressed: _positiveActionPerform,
                  child: Text(
                    "OK",
                    style: GoogleFonts.poppins(
                      color: blueColor,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  _positiveActionPerform() {
    Navigator.of(context).pop(); // To close the dialog
    widget.okBtnFunction();
  }
}
