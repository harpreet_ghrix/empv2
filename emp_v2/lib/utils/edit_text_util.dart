
import 'package:emp_v2/utils/validate.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

import 'colors_util.dart';

class TextFieldUtil extends StatefulWidget {
  final String label;
  final String message;
  final bool obsecureText;
  TextEditingController controller = TextEditingController();

  TextFieldUtil({
    Key key,
    this.controller,
    this.label = "",
    this.message = "",
    this.obsecureText = false,
  }) : super(key: key);

  @override
  _TextFieldUtilState createState() => new _TextFieldUtilState();
}

class _TextFieldUtilState extends State<TextFieldUtil>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
        controller: widget.controller,
        obscureText: widget.obsecureText,

        style: GoogleFonts.poppins(color: Colors.black, fontSize: 14.sp
            ),
        validator: (value) {
          return Validate.requiredField(value,widget.message);
        },
        decoration: InputDecoration(

            contentPadding:
            EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(15.0),borderSide: BorderSide(color: grayColor) ),
            labelText: widget.label,
            labelStyle: GoogleFonts.poppins(fontSize: 15.sp)),
      ),
    );
  }
}
