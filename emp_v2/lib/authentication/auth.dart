import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:emp_v2/utils/exceptions.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Api {
  static const String BASE_URL = 'http://emp.ghrixlabs.com:8090/api/v1/auth/';
}

class Auth with ChangeNotifier {
  var mainUrl = Api.BASE_URL;

  // var AuthKey = Api.authKey;
  var token = '';
  static const register = 'register/';
  static const login = 'login/';
  static const forgot_pass = 'forgotPassword/';
  static const reset_pass = 'reset-password/';
  static const change_pass = 'change-password/';
  static const logut = 'logout/';

  Future<http.Response> forgotPass(email) async {
    var responseJson;
    try {
      var headers = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      };
      var data = {"email": email};
      final http.Response response = await http.post(
          Uri.parse('$mainUrl$forgot_pass'),
          headers: headers,
          body: jsonEncode(data));
      print("Result: ${response.statusCode}");
      //return response.body;

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<http.Response> resetPass(email, otp, new_password) async {
    var responseJson;
    try {
      var headers = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      };
      var data = {'email': email, 'otp': otp, 'new_password': new_password};
      final http.Response response = await http.post(
          Uri.parse('$mainUrl$reset_pass'),
          headers: headers,
          body: jsonEncode(data));
      print("Result: ${response.statusCode}");

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<http.Response> logout() async {
    var responseJson;
    try {
      Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      token = prefs.getString('TOKEN');

      var headers = {
        "Accept": "application/json",
        "Content-Type": "application/x-www-form-urlencoded",
        'Authorization': 'Bearer $token'
      };

      final http.Response response = await http.get(
        Uri.parse('$mainUrl$logut'),
        headers: headers,
      );
      print("Result: ${response.statusCode}");
      //return response.body;

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  static Future<String> getToken() async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    var token = prefs.getString('TOKEN');

    print("token: ${token}");
    return token;
  }

  static Map<String, String> getTokenHeaders() {
    Map<String, String> headers = new Map();

    headers['Authorization'] = 'Bearer yG0FUrAoQErjXC7SCAYIce4Fn84xq9}';
    headers['content-type'] = 'application/json';
    return headers;
  }

  /*Get API*/
  Future<http.Response> getData(String url) async {
    var responseJson;
    try {
      Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      token = prefs.getString('TOKEN');

      print("token: ${token}");
      var headers = {'Authorization': 'Bearer $token'};
      var request = http.Request('GET', Uri.parse('$mainUrl$url'));
      print("token: $mainUrl$url");
      request.headers.addAll(headers);
      http.Response response =
          await http.Response.fromStream(await request.send());
      print("Result: ${response.body}");
      //return response.body;

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<http.Response> getDataParam(
      String url, Map<String, String> params) async {
    var responseJson;
    try {
      Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      token = prefs.getString('TOKEN');

      print("token: ${token}");
      var headers = {'Authorization': 'Bearer $token'};
      var request = http.Request('GET', Uri.parse('$mainUrl$url'));
      print("token: $mainUrl$url");
      request.bodyFields = params;
      request.headers.addAll(headers);
      http.Response response =
          await http.Response.fromStream(await request.send());
      print("Result: ${response.body}");
      //return response.body;

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  /*Post API*/

  Future<http.Response> addData(String url, Map<String, String> params) async {
    var responseJson;
    try {
      Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      token = prefs.getString('TOKEN');
      print("token: ${token}");

      /* if(token=='') {
        var headers = {
          "Accept": "application/json",
          "Content-Type": "application/x-www-form-urlencoded",
          'Authorization': 'Bearer $token'
        };
      }*/
      var headers = {'Authorization': 'Bearer $token'};
      var request = http.Request('POST', Uri.parse('$mainUrl$url'));
      print("URL: $mainUrl$url");
      request.bodyFields = params;
      request.headers.addAll(headers);
      http.Response response =
          await http.Response.fromStream(await request.send());
      print("Result: ${response.body}");

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<http.Response> addRawData(String url, params) async {
    var responseJson;
    try {
      Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      token = prefs.getString('TOKEN');
      print("token: ${token}");

      var headers = {
        'Content-Type': "application/json",
        'Authorization': 'Bearer $token'
      };
      // var headers = {'Authorization': 'Bearer $token'};
      var request = http.Request('POST', Uri.parse('$mainUrl$url'));
      request.body = json.encode(params);
      request.headers.addAll(headers);
      http.Response response =
          await http.Response.fromStream(await request.send());
      print("Result: ${response.statusCode}");

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  /*Put API*/
  Future<http.Response> updateData(
      String url, Map<String, String> params) async {
    var responseJson;
    try {
      Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      token = prefs.getString('TOKEN');

      var headers = {'Authorization': 'Bearer $token'};
      var request = http.Request('PUT', Uri.parse('$mainUrl$url'));
      request.bodyFields = params;
      request.headers.addAll(headers);
      http.Response response =
          await http.Response.fromStream(await request.send());
      print("Result: ${response.statusCode}");
      //return response.body;

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  /*Delete API*/
  Future<http.Response> deleteData(
      String url, Map<String, String> params) async {
    var responseJson;
    try {
      Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      token = prefs.getString('TOKEN');

      var headers = {'Authorization': 'Bearer $token'};
      var request = http.Request('DELETE', Uri.parse('$mainUrl$url'));
      request.bodyFields = params;
      request.headers.addAll(headers);
      http.Response response =
          await http.Response.fromStream(await request.send());
      print("Result: ${response.statusCode}");
      //return response.body;

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<http.Response> uploadPhoto(
      filepath, url, Map<String, String> params) async {
    print("params: $params $url");
    var responseJson;
    try {
      Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
      final SharedPreferences prefs = await _prefs;
      token = prefs.getString('TOKEN');
      var headers = {
        'Authorization': 'Bearer $token',
        'Content-Type': "multipart/form-data",
      };

      var request = http.MultipartRequest('POST', Uri.parse('$mainUrl$url'));
      // request.files.add(await http.MultipartFile.fromPath('files', filepath));
      if (filepath != '') {
        request.files.add(http.MultipartFile(
            'files',
            File(filepath).readAsBytes().asStream(),
            File(filepath).lengthSync(),
            filename: filepath.split("/").last));
      }
      request.headers.addAll(headers);
      request.fields.addAll(params);
      http.Response response =
          await http.Response.fromStream(await request.send());
      print("Result: ${response.body}");
      //return response.body;

      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<String> uploadImage(filename, url, Map<String, String> params) async {
    var headers = {
      'Authorization': 'Bearer $token',
      'Content-Type': "multipart/form-data",
    };
    var request = http.MultipartRequest('POST', Uri.parse('$mainUrl$url'));
    request.files.add(await http.MultipartFile.fromPath('files', filename));
    request.headers.addAll(headers);
    request.fields.addAll(params);
    var res = await request.send();
    return res.reasonPhrase;
  }

  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        return response;
      case 400:
        throw response.body;
      case 401:
      case 403:
        throw UnauthorisedException(response.reasonPhrase);
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode: ${response.statusCode}');
    }
  }
}
