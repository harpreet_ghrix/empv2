class HomeResponseData {
  final int status;
  final String messageType;
  final String message;
  final dynamic data;


  HomeResponseData({this.status, this.messageType, this.message,this.data});



  factory HomeResponseData.fromJson(Map<String, dynamic> json) {
    return HomeResponseData(
      status: json['status'],
      messageType: json['messageType'],
      message: json['message'],
      data: json['data'],


    );
  }
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["status"] = status;
    map["messageType"] = messageType;
    map["message"] = message;
    map["data"] = data;

    return map;
  }
}

