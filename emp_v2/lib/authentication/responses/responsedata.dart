class ResponseData {
  final int  status;
  final String  messageType;
  final String  message;


  ResponseData({this.status, this.messageType, this.message});



  factory ResponseData.fromJson(Map<String, dynamic> json) {
    return ResponseData(
      status: json['status'],
      messageType: json['messageType'],
      message: json['message'],


    );
  }
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["status"] = status;
    map["messageType"] = messageType;
    map["message"] = message;

    return map;
  }
}

