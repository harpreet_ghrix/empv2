import 'package:emp_v2/login/login.dart';
import 'package:emp_v2/signup/view/signup_page.dart';
import 'package:emp_v2/utils/colors_util.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

class LandingPage extends StatefulWidget {
  @override
  LandingPageState createState() => new LandingPageState();

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => LandingPage());
  }
}

class LandingPageState extends State<LandingPage>
    with SingleTickerProviderStateMixin {
  bool isAnimate = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: darkBlueColor,
      body: Container(
        height: 100.h,
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/images/splash_bg.png'),
              fit: BoxFit.fill),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              'assets/images/logo_mini.png',
              width: 30.w,
            ),
            SizedBox(
              height: 10.h,
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 8.0, vertical: 8.0),
              child: Text(
                'Welcome to EMP',
                textAlign: TextAlign.center,
                maxLines: 2,
                style: GoogleFonts.montserrat(
                  fontSize: 18.sp,
                  fontWeight: FontWeight.w300,
                  color: Colors.white,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                'Ghrix Technologies welcomes you to Employee Management Portal',
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                  fontSize: 12.sp,
                  fontWeight: FontWeight.w300,
                  color: subHeadingColor,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: 100.w,
              height: 50.0,
              decoration: BoxDecoration(
                  color: blueColor, borderRadius: BorderRadius.circular(15.0)),
              margin: EdgeInsets.symmetric(horizontal: 25.0),
              child: InkWell(
                onTap: () async {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SignUpPage()));
                },
                child: Center(
                  child: Text(
                    "Sign Up",
                    style: GoogleFonts.montserrat(
                        fontSize: 14.sp,
                        fontWeight: FontWeight.w300,
                        color: Colors.white),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 30,
            ),
            GestureDetector(
              onTap: () async {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginPage()));
              },
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(children: <TextSpan>[
                  TextSpan(
                      text: "ALREADY HAVE AN ACCOUNT? ",
                      style: GoogleFonts.poppins(
                          color: Colors.white,
                          fontWeight: FontWeight.w300,
                          fontSize: 12.sp)),
                  TextSpan(
                      text: "LOG IN",
                      style: GoogleFonts.poppins(
                        color: blueColor,
                        fontSize: 12.sp,
                        fontWeight: FontWeight.bold,
                      )),
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }

  void navigationPage() {}

/* void isAdmin(loginStatus)  {
    SharedPreferencesHelper.getUserType().then((value) {
      if (loginStatus) {
        print("isAdmin status ${value}");
        if (value=="isAdmin") {

          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage()));
        } else {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => ListingReport(isAdmin: false,)));
        }
      } else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      }

    });
  }*/
}
