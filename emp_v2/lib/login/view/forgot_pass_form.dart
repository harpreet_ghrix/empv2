import 'dart:convert';

import 'package:emp_v2/authentication/auth.dart';
import 'package:emp_v2/utils/colors_util.dart';
import 'package:emp_v2/utils/message_dialog.dart';
import 'package:emp_v2/utils/progress_dialog.dart';
import 'package:emp_v2/utils/validate.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

class ForgotPassPage extends StatefulWidget {
  String emailStr = "";

  @override
  _ForgotPassState createState() => _ForgotPassState();
}

class _ForgotPassState extends State<ForgotPassPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey key = GlobalKey();

  Map response = new Map();
  String email = '';
  String message = '';

  TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: darkBlueColor,
        resizeToAvoidBottomInset: false,
        body: Container(
            height: 100.h,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/splash_bg.png'),
                  fit: BoxFit.fill),
            ),
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 8.0),
                      child: Text(
                        'Forgot Password',
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: GoogleFonts.montserrat(
                          fontSize: 22.sp,
                          fontWeight: FontWeight.w300,
                          color: blueColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        'Please fill your email address in the field below',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w300,
                          color: subHeadingColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: 100.w,
                      height: 55.0,
                      margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                      decoration: BoxDecoration(
                          color: grayColor,
                          borderRadius: BorderRadius.circular(15.0)),
                      child: TextFormField(
                        controller: _emailController,
                        textInputAction: TextInputAction.next,
                        maxLines: 1,
                        validator: (value) {
                          return Validate.validateEmail(value);
                        },
                        style: GoogleFonts.poppins(
                            color: textColor, fontSize: 14.sp),
                        decoration: InputDecoration(
                          hintText: "Email",
                          hintStyle: GoogleFonts.poppins(
                              fontSize: 14.sp, color: subHeadingColor),
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          errorStyle:
                              TextStyle(height: 0, color: Colors.transparent),
                          contentPadding: EdgeInsets.symmetric(
                              vertical: 15.0, horizontal: 10.0),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: 100.w,
                      height: 55.0,
                      decoration: BoxDecoration(
                          color: blueColor,
                          borderRadius: BorderRadius.circular(15.0)),
                      margin: EdgeInsets.symmetric(horizontal: 25.0),
                      child: InkWell(
                        onTap: () async {
                          email = _emailController.text.trim();

                          if (Validate.validateEmail(email) != null) {
                            DialogUtils.showCustomDialog(context,
                                title: 'Login',
                                message: Validate.validateEmail(email),
                                okBtnText: "",
                                cancelBtnText: 'OK');
                          } else {
                            forgotPassAPI();
                            print("Successful");
                          }
                        },
                        child: Center(
                          child: Text(
                            "SEND RESET LINK",
                            style: GoogleFonts.montserrat(
                                fontSize: 14.sp,
                                fontWeight: FontWeight.w300,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ],
            )));
  }

  Future forgotPassAPI() async {
    Dialogs.showLoadingDialog(context, key);
    print("Data : $email");
    var param = {"email": email};
    Auth().addData(Auth.forgot_pass, param).then((response) {
      Navigator.of(key.currentContext).pop();
      var jsonResponse = json.decode(response.body);
      print("Response code : ${response.body}");
      if (jsonResponse['success']) {
        DialogUtils.showCustomDialog(context,
            title: 'Forgot Password',
            message: jsonResponse['message'], okBtnFunction: () {
          Navigator.of(context).pop();
        });
      } else {
        DialogUtils.showCustomDialog(
          context,
          title: 'Forgot Password',
          message: jsonResponse['message'].toString(),
        );
      }
    }, onError: (error) {
      Navigator.of(key.currentContext).pop();
      print("Error == $error");
      Map<String, dynamic> data =
          new Map<String, dynamic>.from(json.decode(error));
      DialogUtils.showCustomDialog(context,
          title: 'Error', message: data['message'], okBtnText: "");
    });
  }
}

