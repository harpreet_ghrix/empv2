import 'dart:convert';

import 'package:emp_v2/authentication/auth.dart';
import 'package:emp_v2/login/view/forgot_pass_form.dart';
import 'package:emp_v2/login/view/landing_page.dart';
import 'package:emp_v2/signup/view/signup_page.dart';
import 'package:emp_v2/utils/colors_util.dart';
import 'package:emp_v2/utils/message_dialog.dart';
import 'package:emp_v2/utils/progress_dialog.dart';
import 'package:emp_v2/utils/shared_prefrence.dart';
import 'package:emp_v2/utils/validate.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';

class LoginPage extends StatefulWidget {
  String emailStr = "";

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey key = GlobalKey();

  Map response = new Map();
  String email = '';
  String password = '';
  String message = '';

  String firstname = '';
  String lastname = '';

  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: key,
        backgroundColor: darkBlueColor,
        resizeToAvoidBottomInset: false,
        body: Container(
            height: 100.h,
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/splash_bg.png'),
                  fit: BoxFit.fill),
            ),
            child: Stack(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 8.0),
                      child: Text(
                        'Welcome Back!',
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        style: GoogleFonts.montserrat(
                          fontSize: 22.sp,
                          fontWeight: FontWeight.w300,
                          color: blueColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: Text(
                        'Login with your official ghrix.com email address',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(
                          fontSize: 12.sp,
                          fontWeight: FontWeight.w300,
                          color: subHeadingColor,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Form(
                      key: _formKey,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 100.w,
                            height: 55.0,
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 0),
                            decoration: BoxDecoration(
                                color: grayColor,
                                borderRadius: BorderRadius.circular(15.0)),
                            child: TextFormField(
                              controller: _emailController,
                              textInputAction: TextInputAction.next,
                              maxLines: 1,
                              validator: (value) {
                                return Validate.validateEmail(value);
                              },
                              style: GoogleFonts.poppins(
                                  color: textColor, fontSize: 14.sp),
                              decoration: InputDecoration(
                                hintText: "Email",
                                hintStyle: GoogleFonts.poppins(
                                    fontSize: 14.sp, color: subHeadingColor),
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                errorStyle: TextStyle(
                                    height: 0, color: Colors.transparent),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 15.0, horizontal: 10.0),
                              ),
                            ),
                          ),
                          Container(
                            width: 100.w,
                            height: 55.0,
                            margin: EdgeInsets.fromLTRB(20, 20, 20, 20),
                            decoration: BoxDecoration(
                                color: grayColor,
                                borderRadius: BorderRadius.circular(15.0)),
                            child: TextFormField(
                              controller: _passwordController,
                              obscureText: true,
                              maxLines: 1,
                              validator: (value) {
                                return Validate.validatePass(value);
                              },
                              style: GoogleFonts.poppins(
                                  color: textColor, fontSize: 14.sp),
                              decoration: InputDecoration(
                                hintText: "Password",
                                hintStyle: GoogleFonts.poppins(
                                    fontSize: 14.sp, color: subHeadingColor),
                                border: InputBorder.none,
                                focusedBorder: InputBorder.none,
                                enabledBorder: InputBorder.none,
                                errorBorder: InputBorder.none,
                                disabledBorder: InputBorder.none,
                                errorStyle: TextStyle(
                                    height: 0, color: Colors.transparent),
                                contentPadding: EdgeInsets.symmetric(
                                    vertical: 15.0, horizontal: 10.0),
                              ),
                            ),
                          ),
                          Container(
                            width: 100.w,
                            height: 55.0,
                            decoration: BoxDecoration(
                                color: blueColor,
                                borderRadius: BorderRadius.circular(15.0)),
                            margin: EdgeInsets.symmetric(horizontal: 25.0),
                            child: InkWell(
                              onTap: () async {
                                email = _emailController.text.trim();
                                password = _passwordController.text.trim();
                                if (Validate.validateEmail(email) != null) {
                                  DialogUtils.showCustomDialog(context,
                                      title: 'Login',
                                      message: Validate.validateEmail(email),
                                      okBtnText: "",
                                      cancelBtnText: 'OK');
                                } else if (Validate.validatePass(password) !=
                                    null) {
                                  DialogUtils.showCustomDialog(context,
                                      title: 'Login',
                                      message: Validate.validatePass(password),
                                      okBtnText: "",
                                      cancelBtnText: 'OK');
                                } else {
                                  loginAPI();
                                  print("Successful");
                                }
                              },
                              child: Center(
                                child: Text(
                                  "LOGIN",
                                  style: GoogleFonts.montserrat(
                                      fontSize: 14.sp,
                                      fontWeight: FontWeight.w300,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ForgotPassPage()));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Text(
                          'Forgot Password ?',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            fontSize: 14.sp,
                            fontWeight: FontWeight.w300,
                            color: subHeadingColor,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(20, 0, 20, 30),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignUpPage()));
                      },
                      child: RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                          children: <TextSpan>[
                            TextSpan(
                                text: "NO ACCOUNT ?",
                                style: GoogleFonts.poppins(
                                    color: subHeadingColor,
                                    fontWeight: FontWeight.w300,
                                    fontSize: 12.sp)),
                            TextSpan(
                                text: " CREATE ONE",
                                style: GoogleFonts.poppins(
                                  color: blueColor,
                                  fontSize: 12.sp,
                                  fontWeight: FontWeight.w300,
                                )),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )));
  }

  Future loginAPI() async {
    Dialogs.showLoadingDialog(context, key);
    print("Data : $email $password");
    var param = {"email": email, "password": password};
    Auth().addData(Auth.login, param).then((response) {
      Navigator.of(key.currentContext).pop();
      var jsonResponse = json.decode(response.body);
      print("Response code : ${response.body}");

      if (jsonResponse['success']) {
        SharedPreferencesHelper.saveToken(jsonResponse['access_token']);
        SharedPreferencesHelper.saveUserId(jsonResponse['user']['id']);
        //SharedPreferencesHelper.saveName(jsonResponse['user']['name']);
        SharedPreferencesHelper.saveEmail(jsonResponse['user']['email']);
        SharedPreferencesHelper.isFirstTime(false);
        DialogUtils.showCustomDialog(context,
            title: 'Login',
            message: jsonResponse['message'], okBtnFunction: () {
              Navigator.of(context).pop();
        });
      } else {
        DialogUtils.showCustomDialog(
          context,
          title: 'Login',
          message: jsonResponse['message'].toString(),
        );
      }
    }, onError: (error) {
      Navigator.of(key.currentContext).pop();
      print("Error == $error");
      Map<String, dynamic> data =
          new Map<String, dynamic>.from(json.decode(error));
      DialogUtils.showCustomDialog(
        context,
        title: 'Error',
        message: data['message'] ?? 'Something went wrong',
      );
    });
  }
}
