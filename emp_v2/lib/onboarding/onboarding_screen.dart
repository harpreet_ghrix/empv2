import 'package:emp_v2/login/view/landing_page.dart';
import 'package:emp_v2/login/view/login_page.dart';
import 'package:emp_v2/onboarding/model/onboard.dart';
import 'package:emp_v2/utils/colors_util.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sizer/sizer.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class OnBoardPage extends StatefulWidget {
  @override
  _OnBoardState createState() => _OnBoardState();
}

class _OnBoardState extends State<OnBoardPage> {
  int currentIndex = 0;
  PageController _pageController;
  List<OnboardModel> screens = <OnboardModel>[
    OnboardModel(
      img: 'assets/images/onboarding1.png',
      text: "Welcome to EMP v2",
      desc: "Employee Management portal from Ghrix Technologies Pvt Ltd",
    ),
    OnboardModel(
      img: 'assets/images/onboarding2.png',
      text: "Apply Leave on the Fly",
      desc: "You can apply leave through the mobile app as well",

    ),
    OnboardModel(
      img: 'assets/images/onboarding3.png',
      text: "Get notified on Salary",
      desc: "Get notified when the salary is credited",
    ),
  ];

  @override
  void initState() {
    _pageController = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  _storeOnboardInfo() async {
    print("Shared pref called");
    int isViewed = 0;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('onBoard', isViewed);
    print(prefs.getInt('onBoard'));
  }

  @override
  Widget build(BuildContext context) {
    //final controller = PageController(viewportFraction: 0.8, keepPage: true);

    return Scaffold(
      backgroundColor: darkBlueColor,
      appBar: AppBar(
        backgroundColor: darkBlueColor,
        elevation: 0.0,
        actions: [
          /*TextButton(
            onPressed: () {
              _storeOnboardInfo();
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => LoginPage()));
            },
            child: Text(
              "Skip",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          )*/
        ],
      ),
      body: Stack(
        children: [
          PageView.builder(
              itemCount: screens.length,
              controller: _pageController,
              // physics: NeverScrollableScrollPhysics(),
              onPageChanged: (int index) {
                setState(() {
                  currentIndex = index;
                });
              },
              itemBuilder: (_, index) {
                return Align(
                  alignment: Alignment(1, -0.7),
                  child: Image.asset(
                    screens[index].img,
                    height: index == 2 ? 60.h : 45.h,
                  ),
                );
              }),
          Align(
              alignment: FractionalOffset.bottomCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 8.0, vertical: 8.0),
                    child: Text(
                      screens[currentIndex].text,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      style: GoogleFonts.montserrat(
                        fontSize: 16.sp,
                        fontWeight: FontWeight.w300,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    child: Text(
                      screens[currentIndex].desc,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w300,
                        color: subHeadingColor,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  SmoothPageIndicator(
                    controller: _pageController,
                    count: screens.length,
                    effect: WormEffect(
                      dotHeight: 8,
                      dotWidth: 8,
                      activeDotColor: blueColor,
                      type: WormType.thin,
                      // strokeWidth: 5,
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),

                  InkWell(
                    onTap: () async {
                     // if (currentIndex == 2) {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LandingPage()));
                     // }
                      /*_pageController.nextPage(
                        duration: Duration(milliseconds: 300),
                        curve: Curves.bounceIn,
                      );*/
                    },
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 30.0),
                      padding:
                          EdgeInsets.symmetric(horizontal: 30.0, vertical: 15),
                      decoration: BoxDecoration(
                          color: blueColor,
                          borderRadius: BorderRadius.circular(15.0)),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            Text(
                              "Next",
                              style: GoogleFonts.montserrat(
                                  fontSize: 14.sp, fontWeight: FontWeight.w300,color: Colors.white),
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            Icon(
                              Icons.arrow_forward_sharp,
                              color: Colors.white,
                            )
                          ]),
                    ),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                ],
              )),
        ],
      ),
    );
  }
}
