import 'dart:async';

import 'package:emp_v2/onboarding/onboarding_screen.dart';
import 'package:emp_v2/utils/shared_prefrence.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class SplashPage extends StatefulWidget {
  @override
  SplashState createState() => new SplashState();

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => SplashPage());
  }
}

class SplashState extends State<SplashPage>
    with SingleTickerProviderStateMixin {
  bool isAnimate = false;

  startTime() async {
    var _duration = new Duration(seconds: 3);
    return new Timer(_duration, navigationPage);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) => {startTime()});

    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
          height: 100.h,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/splash_screen.png'),
                fit: BoxFit.fill),
          )),
    );
  }

  void navigationPage() {
    /*SharedPreferencesHelper.getisFirstTime().then((value) {
      print("Is First Time ${value}");
      if (value) {

      } else {}
    });*/

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => OnBoardPage()));
  }

/* void isAdmin(loginStatus)  {
    SharedPreferencesHelper.getUserType().then((value) {
      if (loginStatus) {
        print("isAdmin status ${value}");
        if (value=="isAdmin") {

          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => HomePage()));
        } else {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => ListingReport(isAdmin: false,)));
        }
      } else {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => LoginPage()));
      }

    });
  }*/
}
